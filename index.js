const http = require('http');
const port = process.env.PORT || 8000;


var server = http.createServer(function (request, response) {
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Hello world with Nodejs.");
});


server.listen(8000);

